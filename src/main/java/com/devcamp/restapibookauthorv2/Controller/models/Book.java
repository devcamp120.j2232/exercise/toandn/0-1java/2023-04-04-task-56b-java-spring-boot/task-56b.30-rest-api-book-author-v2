package com.devcamp.restapibookauthorv2.Controller.models;

import java.util.ArrayList;

public class Book {
  private String name;
  private ArrayList<Author> authors;
  private double price;
  private int qty = 0;

  public Book() {
  }

  public Book(String name, ArrayList<Author> authors, double price) {
    this.name = name;
    this.authors = authors;
    this.price = price;
  }

  public Book(String name, ArrayList<Author> authors, double price, int qty) {
    this.name = name;
    this.authors = authors;
    this.price = price;
    this.qty = qty;
  }

  public String getName() {
    return name;
  }

  public ArrayList<Author> getAuthor() {
    return authors;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  @Override
  public String toString() {
    return "Book [name=" + name + ", author=" + authors + ", price=" + price + ", qty=" + qty + "]";
  }

  public String getAuthorNames() {
    String names = "";
    for (Author author : authors) {
      names += author.getName() + ", ";
    }
    return names;
  }
}
