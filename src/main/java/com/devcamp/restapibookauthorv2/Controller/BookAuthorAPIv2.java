package com.devcamp.restapibookauthorv2.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapibookauthorv2.Controller.models.Author;
import com.devcamp.restapibookauthorv2.Controller.models.Book;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class BookAuthorAPIv2 {
  @GetMapping("/books")
  public ArrayList<Book> getListBook() {
    Author author1 = new Author("Nguyen Van A", "a@gamil.com", 'm');
    Author author2 = new Author("Pham Thi B", "b@gamil.com", 'f');
    Author author3 = new Author("Le Chi C", "c@gamil.com", 'm');
    Author author4 = new Author("Nguyen Van D", "d@gamil.com", 'm');
    Author author5 = new Author("Pham The E", "e@gamil.com", 'f');
    Author author6 = new Author("Le Vinh F", "f@gamil.com", 'm');
    System.out.println(author1.toString());
    System.out.println(author2.toString());
    System.out.println(author3.toString());
    System.out.println(author4.toString());
    System.out.println(author5.toString());
    System.out.println(author6.toString());

    ArrayList<Author> authorlist1 = new ArrayList<Author>();
    ArrayList<Author> authorlist2 = new ArrayList<Author>();
    ArrayList<Author> authorlist3 = new ArrayList<Author>();

    authorlist1.add(author1);
    authorlist1.add(author2);
    authorlist2.add(author3);
    authorlist2.add(author4);
    authorlist3.add(author5);
    authorlist3.add(author6);

    Book book1 = new Book("Yoga", authorlist1, 100000, 1);
    Book book2 = new Book("Math", authorlist2, 120000, 2);
    Book book3 = new Book("Piano", authorlist3, 90000, 1);

    ArrayList<Book> books = new ArrayList<Book>();
    books.add(book1);
    books.add(book2);
    books.add(book3);

    return books;
  }
}
